'''
 Reclus: forms
 Includes the description of system & user forms
'''

from flask_wtf import FlaskForm
from wtforms import validators, StringField, BooleanField, TextField, PasswordField, RadioField, \
                                DateField, SubmitField
from wtforms.validators import DataRequired, Email, InputRequired, EqualTo, NumberRange, URL, Length
from wtforms.widgets import TextArea

# Login / registering forms
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired(), Length(max=64)])
    password = PasswordField('Password', validators=[InputRequired(), Length(max=120)])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired(), Length(max=64)])
    email = StringField('Email', validators=[InputRequired(), Email(), Length(max=120)])
    password = PasswordField('Password', validators=[
                                            Length(max=120),
                                            InputRequired(),
                                            EqualTo('confirm',
                                            message='Passwords must match unfortunately =)')])
    confirm = PasswordField('Confirm Password', validators=[InputRequired(), Length(max=120)])
    submit = SubmitField('Register')


# AEMP-specific forms
# Housing Justice Action Form

class HausJusticeForm(FlaskForm):
    page_title = 'Ações em Defesa da Moradia durante a Pandemia'
    form_title = 'Ações em Defesa da Moradia durante a Pandemia'
    form_intro = '''Estamos coletando e mapeando informações para ajudar nas campanhas de  \
                    defesa da moradia durante a pandemia do Coronavírus. Ajude o projeto   \
                    preenchendo o formulário abaixo.'''

    Strike_Type = RadioField('Qual tipo de ação em defesa do direito à moradia você está participando?',
                                validators=[InputRequired('[* Este campo é requerido ]')],
                                    choices=[
                                        ('Suspensão de aluguel', 'Greve, redução ou suspensão de aluguel'),
                                        ('Ocupação', 'Ocupação'),
                                        ('Ajuda mútua', 'Ajuda mútua (ex.: rede de solidariedade local)'),
                                        ('Campanha', 'Campanha (ex.: apoio às familias sem-teto)'),
                                        ('Outra', 'Outra')],
                                        default='Suspensão de aluguel')

    Strike_Status = RadioField('Esta ação está ocorrendo no momento?',
                                        validators=[InputRequired('[* Este campo é requerido ]')],
                                            choices=[
                                                ('Yes / Sí / 是 / Oui', 'Sim'),
                                                ('Unsure / No estoy segurx / 不确定 / Pas sûr.e.s.', 'Não')],
                                                default='Yes / Sí / 是 / Oui')

    Start = StringField('Data de inicio da ação:', validators=[DataRequired('* Este campo é requerido '),
                                                               Length(min=0, max=10,
                                                               message='(máximo 10 caracteres)')])

    Location = StringField('Onde está ocorrendo a ação?', validators=[DataRequired('* Este campo é requerido '),
                                                                      Length(max=120,
                                                                             message='(máximo 120 caracteres)')])

    Who = TextField('Quem está realizando a ação?', validators=[DataRequired('* Este campo é requerido '),
                                                                Length(max=120,
                                                                       message='(máximo 120 caracteres)')])

    Why = TextField('Descreva maiores detalhes sobre a ação e o motivo pelo qual você decidiu participar:',
                        id='form-textbox',
                        render_kw={"onkeyup": "count();"},
                        validators=[DataRequired('* Este campo é requerido '),
                                    Length(min=100, max=600,
                                    message='(digite uma descrição entre 100 e 600 caracteres)')],
                                    widget=TextArea())

    Desire_Contact = RadioField('Esta ação está aberta para ajuda de voluntários ou voluntárias?',
                                coerce=bool, choices=[(True, 'Sim'),
                                                      (False, 'Não')],
                                                      default='Sim')

    Resources = StringField('Digite o endereço de um site ou rede social com informações sobre como apoiar a ação:',
                                validators=[InputRequired('* Este campo é requerido '),
                                            URL(require_tld=True, message='(preencha com um endereço válido) '),
                                            Length(max=255, message='(máximo 255 caracteres)')])

    Email = StringField('Digite seu email (caso queira entrar em contato com a gente):',
                            validators=[validators.optional(),
                                        Email('* Utilize um email válido '),
                                        Length(max=120, message='(máximo 120 caracteres)')])

    submit = SubmitField('Enviar')


# Protection Legislation Form

class LegisProtectionForm(FlaskForm):
    page_title = 'Coronavírus: Legislação de Proteção à Inquilinos'
    form_title = 'Coronavírus: Legislação de Proteção à Inquilinos'
    form_intro = '''Estamos mapeando as leis e decretos que cidades, estados, e o  \
                    governo federal estão passando (ou que estão para serem        \
                    aprovadas) para proteger os inquilinos que não tem condições   \
                    de pagar aluguel durante a pandemia do Coronavírus. Precisamos \
                    da sua ajuda para construir este mapa de solidariedade!'''

    email = StringField('Digite seu email (caso queira entrar em contato):',
                        validators=[validators.optional(),
                        Email('* Utilize um email válido '),
                        Length(max=120, message='(máximo 120 caracteres)')])

    municipality = StringField('Cidade em que a política foi proposta:',
                                validators=[Length(max=120, message='(máximo 120 caracteres)')])

    state = StringField('Estado em que a política foi proposta:',
                         validators=[Length(max=120, message='(máximo 120 caracteres)')])

    admin_scale = RadioField('Escala administrativa da política pública:',
                                validators=[InputRequired('[* Este campo é requerido ]')],
                                choices=[('City', 'Municipal'),
                                         ('State', 'Estadual'),
                                         ('Country', 'Federal')],
                                         default='City')

    passed = RadioField('Esta legislação ou política pública já foi aprovada?',
                           validators=[InputRequired('[* Este campo é requerido ]')],
                           choices=[('TRUE', 'Sim'),
                                    ('FALSE', 'Não')],
                                    default='TRUE')

    policy_type = RadioField('Qual é o instrumento ou orgão responsável pela política pública?',
                                validators=[InputRequired('[* Este campo é requerido ]')],
                                choices=[('1', 'Conselho municipal'),
                                         ('2', 'Conselho regional'),
                                         ('3', 'Conselho estadual'),
                                         ('4', 'Secretaria de habitação'),
                                         ('5', 'Ordem judicial'),
                                         ('6', 'Ordem executiva municipal'),
                                         ('7', 'Ordem executiva estadual'),
                                         ('8', 'Ordem executiva federal'),
                                         ('9', 'Legislação municipal'),
                                         ('10', 'Legislação estadual'),
                                         ('11', 'Legislação federal'),
                                         ('12', 'Outro')],
                                         default='1')

    policy_summary = TextField('Descrição da política pública:', widget=TextArea(),
                               id='form-textbox',
                               render_kw={"onkeyup": "count();"},
                               validators=[InputRequired('* Este campo é requerido '),
                                           Length(min=100, max=600,
                                           message='(digite uma descrição entre 100 e 600 caracteres)')])

    start = StringField('Data de inicio:', validators=[DataRequired('* Este campo é requerido '),
                                                       Length(min=0, max=10,
                                                       message='(máximo 10 caracteres)')])

    end = StringField('Data de término:', validators=[validators.optional(), Length(min=0, max=10,
                                                                             message='(máximo 10 caracteres)')])

    link = StringField('Digite aqui o endereço de um site com maiores informações sobre a lei ou política pública:',
                            validators=[InputRequired('* Este campo é requerido '),
                                        URL(require_tld=True, message='Preencha com um endereço válido'),
                                        Length(max=255, message='Máximo 255 caracteres')])

    resources = StringField('Liste aqui as organizações de apoio de sua região na luta pela  moradia:',
                            validators=[Length(max=255, message='(máximo 255 caracteres)')])

    feedback = StringField('Deixe sugestões para o nosso projeto (caso queira ajudar):',
                            validators=[Length(max=255, message='(máximo 255 caracteres)')])

    submit = SubmitField('Enviar')
