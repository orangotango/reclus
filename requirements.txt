alembic==1.4.2
arrow==0.15.5
Babel==2.8.0
bcrypt==3.1.7
binaryornot==0.4.4
blinker==1.4
certifi==2020.4.5.1
cffi==1.14.0
chardet==3.0.4
click==7.1.1
cookiecutter==1.7.2
dnspython==1.16.0
dominate==2.5.1
email-validator==1.0.5
Flask==1.1.2
Flask-Admin==1.5.6
Flask-Babel==1.0.0
Flask-BabelEx==0.9.4
Flask-Bootstrap==3.3.7.1
Flask-Ext==0.1
Flask-HTTPAuth==4.1.0
Flask-Login==0.5.0
Flask-Mail==0.9.1
flask-marshmallow==0.13.0
Flask-Migrate==2.5.3
Flask-Principal==0.4.0
Flask-Security-Too==3.4.1
Flask-SQLAlchemy==2.4.3
Flask-WTF==0.14.3
geographiclib==1.50
geopy==2.0.0
idna==2.9
itsdangerous==1.1.0
Jinja2==2.11.2
jinja2-time==0.2.0
Mako==1.1.3
MarkupSafe==1.1.1
marshmallow==3.6.1
marshmallow-sqlalchemy==0.23.1
mimerender==0.6.0
passlib==1.7.2
Pillow==6.1.0
pip-autoremove==0.9.1
poyo==0.5.0
psycopg2==2.8.5
pycparser==2.20
python-dateutil==2.8.1
python-dotenv==0.13.0
python-editor==1.0.4
python-mimeparse==1.6.0
python-slugify==4.0.0
pytz==2019.3
requests==2.23.0
six==1.14.0
speaklater==1.3
SQLAlchemy==1.3.17
text-unidecode==1.3
urllib3==1.25.9
uWSGI==2.0.19.1
visitor==0.1.3
Werkzeug==0.16.0
WTForms==2.2.1
